
const routes = [
  {
    path: '/',
    redirect:{
      name:'my-tasks'
    },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
      path: 'my-tasks',
      name: 'my-tasks',
      component: () => import('pages/IndexPage.vue')
    },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
